module testsqlitexorm

go 1.15

require (
	modernc.org/sqlite v1.8.1
	xorm.io/xorm v1.0.6
)
