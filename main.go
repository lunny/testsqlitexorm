package main

import (
	"fmt"
	"log"

	"xorm.io/xorm"
	"xorm.io/xorm/dialects"

	_ "modernc.org/sqlite"
)

type Person struct {
	Id   int64
	Name string
	Age  int
}

func main() {
	dialects.RegisterDriver("sqlite", dialects.QueryDriver("sqlite3"))
	engine, err := xorm.NewEngine("sqlite", "data.db")
	if err != nil {
		log.Fatal(err)
	}

	err = engine.Sync2(new(Person))
	if err != nil {
		log.Fatal(err)
	}

	_, err = engine.Insert(&Person{
		Name: "xlw",
		Age:  20,
	})
	if err != nil {
		log.Fatal(err)
	}

	var persons []Person
	err = engine.Find(&persons)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(persons)
}
